APP_NAME = 'Practoline.com'
APP_URL = 'http://www.practoline.com'


CONTACT_EMAIL = 'practoline@hailisys.com'

IMAGE_CDN_BASE = "https://s3.amazonaws.com/practolines/images/apps"
#IMPORTATN: DO NOT USE DOUBLE LINE FOR KEYWORDS

META_TITLE = "Doctors & Diagnostic Centers: Address, Contacts & Fees - #{APP_NAME}"
META_KEYWORDS = "doctors clinic hospital labs test diagnostic centre"
META_DESCRIPTION = "Your one stop destination for health care. Find top doctors, clinics and diagnostic centre with camparative fees"

PAGE_LIMIT = 12
MOBILE_PAGE_LIMIT = 5


FACEBOOK_PAGE = "https://www.facebook.com/practoline"
TWITTER_PAGE = "https://twitter.com/practoline"
GOOGLE_PAGE = "https://plus.google.com/112503757859686056809/posts"
