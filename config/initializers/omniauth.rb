module Google
  CONFIG  = YAML.load_file(Rails.root.join("config/google.yml"))[Rails.env]
  APP_ID  = CONFIG['client_id']
  SECRET  = CONFIG['client_secret']
  API_KEY = CONFIG['api_key']
end

Rails.application.config.middleware.use OmniAuth::Builder do
	provider :google_oauth2, Google::APP_ID, Google::SECRET,
	{
	  :name => "google",
	  :scope => "email, profile, plus.me, http://www.google.com/m8/feeds/",
	  access_type: "offline",
	  :include_granted_scopes => true
	}
end