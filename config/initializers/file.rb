class ActiveRecord::Base
	class << self


    def db
      self.connection
    end
    

    def underscore
      self.to_s.underscore
    end
   
		# helper for dubugging sql
	  def to_file(filename, text)
	    File.open(filename, 'a+') do |f|
	      f.puts text
	    end
	  end

  end

  def to_url(host=nil)
  	url = self.is_a?(User) ? "/#{self.username.downcase}": "/#{self.class.underscore}/#{self.title.to_s.parameterize}/#{id}"
    host ? "#{host}#{url}" : url
  end
  	
  def generate_token
    token = ''
    10.times { token << (j = Kernel.rand(62); j += ((j < 10) ? 48 : ((j < 36) ? 55 : 61 ))).chr }
    token
  end

end      