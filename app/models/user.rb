class User < ActiveRecord::Base
  
  ROLES = {:user => 1, :doctor => 2, :admin => 3}
  GENDERS = {:male => 1, :female => 2}

  attr_accessor :password

  before_create :set_default_role

  before_save :encrypt_password

  after_create :create_username

  # rake paperclip:refresh:missing_styles CLASS=YourModelName
  # image.reprocess! - updates images
  # image.reprocess! :cover - only update 'cover' style

  has_attached_file(
    :image, 
    :styles => {:medium => "70x80>", :show => "200x210>", :small => "45x45>", :cover => "5x5>"},
    :storage => :s3,
    :s3_credentials => "#{Rails.root}/config/s3.yml",
    :path => "images/users/:style/:id/:filename"
  )
  
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates_uniqueness_of :username

  class << self

  	def authenticate(email, password)
		sha1pass = sha1(password)
	    where("email = #{email} AND encrypted_password = sha1pass").first rescue nil
  	end

  	def sha1(str)
  		Digest::SHA1.hexdigest("lvugsdqx#{str}nmer")
  	end

  end

  def meta_title(options={})
    tt = "#{first_name}'s playlists"
    tt << " - #{Playlist.find(options[:list]).title}" if options[:list]
    tt
  end

  def just_first_name
    name.split(' ').first
  end

  def meta_keywords
    "#{name} Profile, playlist, jukebox"
  end

  def meta_description
    description.to_s
  end

  def itemtype
    "http://schema.org/Person"
  end

  def first_name
    name.split(' ').first
  end

  def title_info(length=nil)
    name
  end

  def title
    name
  end


  def admin?
    self.role_id == 3
  end

  def encrypt_password
  	self.encrypted_password = Digest::SHA1.hexdigest("lvugsdqx#{password}nmer") unless password.blank?
  end

  def set_default_role
  	self.role_id = 1
  end

  def create_username
    login = self.email.split('@').first
    login = login + id.to_s  if User.find_by_username(login) and self.new_record?
    self.update_attributes!({username: login})
  end

end
