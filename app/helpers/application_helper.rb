module ApplicationHelper
  
  def link_to_item(item, title=nil, options={})
    title ||= item.title.blank? ? 'Not available' : item.title_info(options[:length])
    link_to(title, item.to_url, options)
  end

  def link_to_image(item, action='show',options={})
    pt = (item.image_file_name.nil? rescue false) ? 'pt-5' : ''
    link_to("<img src='#{image_url(item, action)}' class='#{action} #{options[:class]} #{pt}'/>".html_safe, item.to_url)
  end

  def image_url(item,action='show')
    return item.image(action) unless item.image_file_name.nil? rescue true
    "https://s3.amazonaws.com/mymster/images/others/blank.png"
  end

  def link_to_app
    %Q(<a href="#{APP_URL}">#{APP_NAME}</a>).html_safe
  end

  def init_koala
    url = request.host.include?('localhost') ? 'http://localhost:3000' : request.host
    session[:oauth] = Koala::Facebook::OAuth.new(Facebook::API_KEY, Facebook::SECRET, "http://#{url}/user/connect")
  end
  
  def facebook_authentication_url
    init_koala if session[:oauth].nil?
    session[:oauth].url_for_oauth_code(:permissions=>"read_stream, email")
  end
  
  def fb_graph
    controller.send('fb_graph')
  end

  def title
    item_title
  end
  
  def item_title
    @item ? "#{@item.title_info}" : "#{list_title}"
  end

  def item_image
    @item ? image_url(@item, 'original') : "#{IMAGE_CDN_BASE}/apple-touch-icon.png"
  end

  def list_title
    META_TITLE
  end

  def meta_title
    @item ? @item.meta_title : default_meta_title
  end
  
  def meta_keywords
    @item ? @item.meta_keywords : default_meta_keywords    
  end
  
  def meta_description
    @item ? truncate(@item.meta_description, :length => 160) : default_meta_description
  end
  
  def default_meta_title
    META_TITLE
  end
  
  def default_meta_keywords
    META_KEYWORDS
  end

  def default_meta_description
    META_DESCRIPTION
  end

  def facebook_like_button
    %Q(
      <div class="fb-like" data-href="http://www.facebook.com/mymster" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
    ).html_safe
  end

  def facebook_js
    %Q(
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=#{Facebook::API_KEY}";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
    ).html_safe
  end

  def google_analytics_js
     %Q(
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-69223531-1', 'auto');
        ga('send', 'pageview');

      </script>
      ).html_safe
  end
  
  def cache_key
    keys = params.dup
    keys.delete('ip_address')
    keys.values.join('-')
  end

end
