$.ajaxSetup({   
  'beforeSend': function(xhr) {
    if(window.location.pathname.split('/')[1] != 'upload'){xhr.setRequestHeader("Accept", "text/javascript")}},
  'cache': false
});
function hideNotification(timing){
	$('#flash').fadeOut(1000);//slideUp(1500)
}

function showNotification(text){
	$("#flash").html(text + "<script>	setTimeout(hideNotification, 2000);</script>")
	$("#flash").fadeIn();
}


function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?|&])" + key + "=.*?(&|$)", "i");
  separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}

showActivity = false;
$(document).bind('ajaxSend', function() {
  if (window.location.pathname.split('/')[1] != 'upload' && showActivity){
	 $.fancybox.showActivity()
  }
});
$(document).bind('ajaxComplete', function() {
	$.fancybox.hideActivity()
	$(".fancybox").fancybox();
});

$(function() {
  //$('textarea').autogrow();
	setTimeout(hideNotification, 5000);	
	$(".fancybox").fancybox();
});

function initTabAction(){
  $('.ui-tabs-nav a').click(function(){
    var tab =  $('.ui-state-active').children()[0].href.split('#')[1]//this.attr('href')
    if($('#'+tab+'_loaded').html() != 'loaded'){
      var id = $('#'+tab).attr('data-id');
      var type = $('#'+tab).attr('data-type');
      $.get('/home/content_'+tab+'?id='+id+"&type="+type);
      $('#'+tab+'_loaded').html('loaded');
    }
  });
}
function initControlActions(){
    $('.item').hover(
      function () { //appearing on hover
        $("#controls-"+this.id).show();
      },
      function () { //disappearing on hover
        $("#controls-"+this.id).hide();
      }
    );
}
function initAdminActions(){
    $('.admin').mouseenter(
      function () { //appearing on hover
        $('.admin-menu').hide();
        $("#menu-"+this.id).fadeIn();
      }
    );

    $('.admin-menu').mouseleave(
      function () { //appearing on hover
        $(this).fadeOut();
      }
    );
}

function initPlaylistActions(){
    $('.playlist').mouseenter(
      function () { //appearing on hover
        $('.pmenu').hide();
        showActivity = false;
        $("#menu-"+this.id).fadeIn();
        $.ajax({url: "/user/playlist?id="+this.id.split('-')[1]});
        showActivity = true;
        //$('#overlay').show();
      }
    );

    $('.pmenu').mouseleave(
      function () { //appearing on hover
        $(this).fadeOut();
        $('#overlay').hide();
      }
    );
}
$(document).ready(
  /* This is the function that will get executed after the DOM is fully loaded */


  function () {
    $('#similar').ready(function(){
      showActivity = false;
      var id = $('#similar').attr('data-id');
      var type = $('#similar').attr('data-type');
      if(id != undefined){
        $.ajax({url: "/"+type+"/similar/"+id});
      }
      showActivity = true;
    });

    $('#filter').change(function(){
      var filter = $('#filter').val();
      var order = filter.split('&')[1].split('=')[1]
      var sort = filter.split('&')[0].split('=')[1]
      var url = updateQueryStringParameter(document.URL, 'sort', sort);
      url = updateQueryStringParameter(url, 'order', order);
      if(filter.split('&')[2]){
        var images = filter.split('&')[2].split('=')[1]  
        url = updateQueryStringParameter(url, 'images', images);
      }
      
      window.location = url
    });

    $('#language').change(function(){
      var language_id = $('#language').val();
      var url = updateQueryStringParameter(document.URL, 'language_id', language_id);
      window.location = url
    });

    $('#category').change(function(){
      var genre_id = $('#category').val();
      var url = updateQueryStringParameter(document.URL, 'genre_id', genre_id);
      window.location = url
    });

    // Get references to our main DOM items.
    var criteria = $("#keyword");
    var results = $("#results");
    // Turn off auto-complete on the criteria input.
    criteria.attr( "autocomplete", "off" );
    var xhr = null;
    var resultsTimer = null;
    var show = false;
    showSpinner = false
    var getResults = function( query ){
      // Get the results.
      xhr = $.ajax({
        type: "get",
        url: "/home/autocomplete",
        data: {query: query},
        dataType: "html",
        success: function(response){
          results.html(response);
          if (response.length){show = true;results.show();} 
          else {show = false;results.hide();}
          $('#close').show();
        },
      });
    };


    // Bind the key up events in the input.
    criteria.keyup(
      function( event ){
        clearTimeout( resultsTimer );
        if (xhr){xhr.abort();}
        var query = criteria.val();
        if(query.length > 2){
          resultsTimer = setTimeout(function(){getResults( query )}, 150);
        }
        else{show=false;results.hide();}
      }
    );
    //criteria.blur(function(event){results.hide();return true;});
    criteria.focus(function(event){if(show){results.show();}else{results.hide()}}); 


    $('.nav').mouseenter(
      function () { //appearing on hover
        $('.popover').fadeOut();
        $('#overlay').show();
        $("#"+this.id+'-menu').fadeIn('slow');
      }
    );

    $('.nav').click(
      function () { //appearing on hover
        $('.popover').fadeOut();
        $('#overlay').show();
        $("#"+this.id+'-menu').fadeIn('slow');
      }
    );

    $('.popover').mouseleave(
      function () { //appearing on hover
        $('.popover').fadeOut();
        $('#overlay').hide();
      }
    );
    
    $('.pmenu').mouseleave(
      function () { //appearing on hover
        $('.pmenu').fadeOut();
        $('#overlay').hide();
      }
    );    
    //$('body').click(function(){$('.popover').fadeOut();});
    $('body').click(function(){$('#results').fadeOut();});
    initControlActions();

    initPlaylistActions();
    initAdminActions();

  }
);