class UserController < ApplicationController

	def profile
		@item = User.find_by_username(params[:username])
	end

	def logout
		cookies.delete(:user)
		redirect_to root_url
	end

	def google

		auth = env["omniauth.auth"]
		email = auth.email
		unless current_user
		  info = auth.extra.raw_info

		  unless @user
		    email = auth.info.email
		    @user = User.create({
		    	email: email,
		    	name: info.name,
		    	gender: User::GENDERS[info.gender.to_sym], 
		    	provider: auth.provider, 
		    	provider_id: auth.uid,
		    	image: info.picture.to_s.gsub("sz=50", "sz=200")
		    })
		  end
		  cookies.permanent[:user] = @user.id
		  #Job::ImportContacts.create(:user_id => @user.id, :token => auth["credentials"]["token"]) rescue nil
		else
		  #Job::ImportContacts.create(:user_id => cookies[:user], :token => auth["credentials"]["token"]) rescue nil
		  #redirect_url = '/invite/google'
		end
		@user = User.find_by_email(email)
		cookies.permanent[:user] = @user.id
		redirect_to root_url

	end
end
