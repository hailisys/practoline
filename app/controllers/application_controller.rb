class ApplicationController < ActionController::Base

  #respond_to :html, :xml, :json, :rjs,:erb, :js
  
  protect_from_forgery
  
  before_filter :parse_params, :prepare_for_mobile, :store_back_url
  before_filter :reset_page_unless_xhr_request, :only => [:index, :show, :profile]
  skip_before_filter  :verify_authenticity_token

  def filters
    @item = Artist.find(params[:id])
    respond_to do |format|
      format.html {render :stream => true, :layout => false}
      format.js {render :content_type => 'text/javascript', :layout => false}
    end
  end

  def languages
    respond_to do |format|
      format.html {render :stream => true, :layout => false}
      format.js {render :content_type => 'text/javascript', :layout => false}
    end
  end

  def genres
    respond_to do |format|
      format.html {render :stream => true, :layout => false}
      format.js {render :content_type => 'text/javascript', :layout => false}
    end
  end

  def times
    respond_to do |format|
      format.html {render :stream => true, :layout => false}
      format.js {render :content_type => 'text/javascript', :layout => false}
    end
  end

  private

  def search_content
    keyword = params[:keyword].to_s.squish
    begin
      @albums  = Album.search(keyword,:order => "visit_counts DESC", :populate => true)
      @songs   = Song.search(keyword,:order => "visit_counts DESC", :populate => true)
      @artists = Artist.search(keyword,:order => "visit_counts DESC", :populate => true)
    rescue Exception => exc
      @albums  = Album.search_like(keyword) rescue []
      @songs   = Song.search_like(keyword) rescue []
      @artists = Artist.search_like(keyword) rescue []
    end
    search = Search.find_or_create_by({title: keyword}) rescue nil
    if search 
      search.update_attributes(
        :ip_address => request.remote_ip, 
        :matches => (@albums.size + @songs.size + @artists.size), 
        :tries => search.tries.to_i + 1
      )
    end
    
  end

  def fb_graph
    @fb_graph ||= Koala::Facebook::API.new(session[:access_token])
  end

  def current_user
    @current_user = User.find_by_id(cookies[:user])
  end
  helper_method :current_user

  def admin?
    return false unless current_user
    current_user.admin?
  end
  
  def mobile_device?
    if session[:mobile_param]
      session[:mobile_param] == "1"
    else
      #request.user_agent =~ /Mobile|webOS/
      /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.match(request.user_agent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.match(request.user_agent[0..3]) rescue false
    end
  end

  def language_text
    cookies[:language].to_s.downcase
  end

  # Return true if the user agent is a bot.
  def robot?
    bot = /(Baidu|bot|Google|SiteUptime|Slurp|WordPress|ZIBB|ZyBorg)/i
    request.user_agent =~ bot
  end

  def smartphone?
    request.env["HTTP_USER_AGENT"] && request.env["HTTP_USER_AGENT"][/(iPhone|iPad|iPod|BlackBerry|Android)/]
  end

  helper_method :mobile_device?
  helper_method :smartphone?
  helper_method :admin?
  helper_method :language_text

  def prepare_for_mobile
    session[:mobile_param] = params[:mobile] if params[:mobile] and (current_user.admin? rescue false)
    prepend_view_path Rails.root + 'app' + 'mobile' if mobile_device?
  end
  
  def content_class
    (model_name || DEFAULT_CONTENT).classify.constantize
  end
  helper_method :content_class
  
  def reset_page_unless_xhr_request
    unless xhr? #and !mobile_device?
      session[:offset] = 0
    end
  end
  
  def model_name(type=params[:type])

    case type
      when 'topliner' then 'Artist'
      when 'stack' then 'Album'
      when 'track' then 'Song'
      else type
    end
  end

  def content
    content_class.find_by_id(params[:id])
  end

  def login_required
    redirect_to '/?login=true' unless current_user #and current_user.admin?
  end

  def home_content
    @items = content_class.list(params)
  end

  def store_back_url
    session[:return_to] = request.referer unless request.xhr?
  end

  def search_language(keyword)
    Language.where("title LIKE ?", "%#{keyword}%")
  end

  def search_genre(keyword)
    Genre.where("title LIKE ?", "%#{keyword}%")
  end
  
  protected

  def parse_params
    params.permit!
    params[:language_id] ||= (Language.find_by_title(params[:language]).id rescue 0) if params[:language]
    params[:genre_id] ||= (Genre.find_by_title(params[:genre].to_s.gsub('%20', ' ')).id rescue 0) if params[:genre]
  end

  def log_visit
    return if robot? # Do not log if visited by bots
    params[:ip_address] = request.remote_ip
    @item.log_visit(params) if @item
  end
  
  def create_album
    content_class.create(params[:album])
  end

  def create_artist
    content_class.create(params[:artist])
  end

  def create_song
    song = content_class.create(params[:song])
    params[:artist_ids].split(',').each do |artist_id|
      next unless Artist.find_by_id(artist_id)
      c = ArtistContent.find_or_create_by(artist_id: artist_id, content_id: song.id)
    end
    song
  end
  
  def update_song
    content.update_attributes(params[:song].merge(:personalized => false))
    content.update_attribute(:parent_id, params[:album_id])
    ArtistContent.delete_all("content_id = #{content.id}")
    params[:artist_ids].split(',').each do |artist_id|
      next unless Artist.find_by_id(artist_id)
      ArtistContent.find_or_create_by(artist_id: artist_id, content_id: content.id)
    end

    content.genres.destroy_all rescue nil # remove existing genres with callbacks
    params[:genre_ids].split(',').each do |genre_id|
      ContentGenre.find_or_create_by(content_id: content.id, genre_id: genre_id)
    end   

    content.languages.destroy_all #rescue nil # remove existing languages with callback
    params[:language_ids].split(',').each do |language_id|
      ContentLanguage.find_or_create_by(content_id: content.id, language_id: language_id)
    end 

    unless params[:video_url].blank?
      content.video.destroy if content.video
      content.import_video(params[:video_url])
    end
  end

  def update_album
    content.update_attributes(params[:album])

    content.songs.each do |s|
      s.languages.clear rescue nil # remove existing languages
      params[:language_ids].split(',').each do |language_id|
        ContentLanguage.find_or_create_by(content_id: s.id, language_id: language_id)
      end 
    end
    
    content.songs.each do |s|
      s.genres.clear rescue nil # remove existing genres
      params[:genre_ids].split(',').each do |genre_id|
        ContentGenre.find_or_create_by(content_id: s.id, genre_id: genre_id)
      end
    end 

  end

  def update_artist
    content.update_attributes(params[:artist])
    
  end

  def update_user
    content.update_attributes(params[:user])
  end

  def set_default_language
    cookies.permanent[:language] = @item ? (@item.languages.first.title rescue cookies[:Language]) : cookies[:language]
    cookies.permanent[:language] = 'hindi' if cookies[:language].blank?
  end

  def xhr?
    request.format.to_s == 'text/javascript'
  end
  
  def page_limit
    mobile_device? ? MOBILE_PAGE_LIMIT : PAGE_LIMIT
  end
  helper_method :page_limit
  
  def not_found
    render :file => "#{Rails.root}/public/404.html",  :status => 404, :layout => false
  end
end
