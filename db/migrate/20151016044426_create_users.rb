class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string  :name
      t.string  :email
      t.string  :username
      t.text    :description
      t.integer :role_id
      t.string  :gender
      t.string  :provider
      t.string  :provider_id
      t.timestamps null: false
    end
    add_attachment :users, :image
    add_index(:users, :username)
    add_index(:users, :email)
    add_index(:users, :role_id)

  end
end
